package employeeSalary;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class User {
	@Override
	public String toString() {
		return "User [id=" + bankAccount + ", username=" + name + ", email=" + address + ", password=" + mobile
				+ ", fullname=" + grade + ", getId()=" + getBankAccount()
				+ ", getUsername()=" + getName() + ", getPassword()=" + getMobile()
				+ ", getFullname()=" + getGrade() + ", getPhone()="
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
				+ "]";
	}
	private int bankAccount,grade;
	private String name, address,mobile;
	private double basicSalary;
	public int getBankAccount() {
		return bankAccount;
	}
	public void setBankAccount(int bankAccount) {
		this.bankAccount = bankAccount;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public int getGrade() {
		return grade;
	}
	public void setGrade(int grade) {
		this.grade = grade;
	}
	public double getBasicSalary() {
		
		
        double baseSalary = ((6 - this.getGrade()) * 5000) + 5000;
	    double allowance = this.calculateAllowance(baseSalary);
	    double basicSalary = baseSalary + allowance;
	    
	    return basicSalary;
		
	}
	public void setBasicSalary(double basicSalary) {
		
		this.basicSalary = basicSalary;
	}
	
    private double calculateAllowance(double baseSalary) {
	        double medicalAllowance = baseSalary * .15; //15% of base
	        double houseRent = baseSalary * .20; //20% of base
	        return medicalAllowance + houseRent;
	    }
}
