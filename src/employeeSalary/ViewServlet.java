package employeeSalary;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
@WebServlet("/ViewServlet")
public class ViewServlet extends HttpServlet{
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<a href='index.html'>Add New User</a>");
		out.println("<h2>User List</h2>");
		List<User> list= UserDao.getAll();
		out.println("<table border='1'>");
		out.println("<tr><th>Name</th><th>Grade</th><th>Mobile</th><th>Address</th><th>Bank Account</th><th>Basic salary</th><th colspan='2'>Action</th></tr>");
		for(User u : list) {
			out.println("<tr><td>"+u.getName()+"</td><td>"+u.getGrade()+"</td><td>"+u.getMobile()+"</td><td>"+u.getAddress()+"</td><td>"+u.getBankAccount()+"</td><td>"+u.getBasicSalary()+"</td><td><a href='EditServlet?bankAccount="+u.getBankAccount()+"'>Edit</a></td><td><a href='DeleteServlet?bankAccount="+u.getBankAccount()+"'>Delete</a></td></tr>");
				
		}
		out.println("</table>");
		out.close();
	}
}
