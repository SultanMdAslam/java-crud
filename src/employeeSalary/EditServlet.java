package employeeSalary;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/EditServlet")
public class EditServlet extends HttpServlet{
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<h3>Update User</h3>");
		String sBankAccount = request.getParameter("bankAccount");
		int bankAccount = Integer.parseInt(sBankAccount);
		User u = UserDao.getInfoById(bankAccount);
		out.println("<form action='UpdateServlet' method='POST'>");
		out.println("<table>");
		out.println("<tr><td>Name : </td><td><input type='text' name='name' value='"+u.getName()+"' /></td></tr>");
		out.println("<tr><td>Grade : </td><td><input type='text' name='grade' value='"+u.getGrade()+"' /></td></tr>");
		out.println("<tr><td>Mobile : </td><td><input type='text' name='mobile' value='"+u.getMobile()+"' /></td></tr>");
		out.println("<tr><td>Address : </td><td><input type='text' name='address' value='"+u.getAddress()+"' /></td></tr>");
		out.println("<tr><td>Bank Account</td><td><input type='text' name='bankAccount' value='"+u.getBankAccount()+"' /></td></tr>");
		out.println("<tr><td colspan='2'><input type='submit'  value='Update' /></td></tr>");
		out.println("</table>");
		out.println("</form>");
		
	}
}
