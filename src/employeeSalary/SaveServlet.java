package employeeSalary;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/SaveServlet")
public class SaveServlet extends HttpServlet{
	
	// HttpServlet is a base class for handling Http request and providing response
	// doPost method handles the POST request
	// request object provides the access to the request information for HTTP servlets
	// can write information about the data it will send back.
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		// getParameter() -> to retrieve the input values from HTML page
		String sbankAccount = request.getParameter("bankAccount");
		int bankAccount = Integer.parseInt(sbankAccount);
		String name = request.getParameter("name");
		String sgrade = request.getParameter("grade");
		int grade = Integer.parseInt(sgrade);
		String address = request.getParameter("address");
		String mobile = request.getParameter("mobile");
		
		User e = new User();
		e.setBankAccount(bankAccount);
		e.setName(name);
		e.setGrade(grade);
		e.setMobile(mobile);
		e.setAddress(address);
		int status = UserDao.save(e);
		if(status>0) {
			out.print("<p> Record Saved Successfully</p>");
			request.getRequestDispatcher("index.html").include(request, response);
		}else {
			out.print("Sorry ! Unable to save record");
		}
		out.close();
	}
	
}
