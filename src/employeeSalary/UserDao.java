package employeeSalary;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class UserDao {
	//Database connection
	public static Connection getConnection() {
		Connection con = null;
				String url= "jdbc:mysql://localhost:3306/employee";
				String user="root";
				String password="#Aslam1993";
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con= DriverManager.getConnection(url, user, password);
			System.out.println("Database connected...");
		}catch(Exception e) {
			System.out.println(e);
		}
		
		return con;
	}
	//Data Insert into user table
	public static int save(User e) {
		int status=0;
		try {
			Connection connection = UserDao.getConnection();
			String sql="INSERT INTO user (name,grade,mobile,address,bankAccount,basicSalary) VALUES (?, ?, ?, ?,?,?)";
			PreparedStatement p = connection.prepareStatement(sql);
			
			p.setString(1, e.getName());
			p.setInt(2, e.getGrade());
			p.setString(3, e.getMobile());
			p.setString(4, e.getAddress());
			p.setInt(5, e.getBankAccount());
			p.setDouble(6, e.getBasicSalary());
			
			 status = p.executeUpdate();
			 connection.close();
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		return status;
		
	}
	public static List<User> getAll(){
		List<User> list = new ArrayList<User>();
		try {
			Connection conection = UserDao.getConnection();
			String sql="SELECT * FROM user";
			PreparedStatement p = conection.prepareStatement(sql);
			ResultSet rs = p.executeQuery();
			
			while(rs.next()) {
				User u = new User();
				u.setName(rs.getString(1));
				u.setGrade(rs.getInt(2));
				u.setMobile(rs.getString(3));
			    u.setAddress(rs.getString(4));
			    u.setBankAccount(rs.getInt(5));			 
			    u.setBasicSalary(rs.getDouble(6));
			    
			    
				
				list.add(u);
				
				
			}
			conection.close();
			
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		return list;
	}
	public static int delete(User u) {
		int status = 0;
		try {
			Connection connection = UserDao.getConnection();
			String sql = "DELETE FROM user WHERE bankAccount=?";
			PreparedStatement p = connection.prepareStatement(sql);
			p.setInt(1, u.getBankAccount());
			status = p.executeUpdate();
			connection.close();
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		return status;
	}
	public static User getInfoById(int bankAccount) {
		User u = new User();
		int status =0;
		try {
			Connection connection = UserDao.getConnection();
			String sql = "SELECT * FROM user WHERE bankAccount=?";
			PreparedStatement p = connection.prepareStatement(sql);
			p.setInt(1, bankAccount);
			ResultSet rs = p.executeQuery();
			if(rs.next()) {
				u.setName(rs.getString(1));
				u.setGrade(rs.getInt(2));
				u.setMobile(rs.getString(3));
				u.setAddress(rs.getString(4));
				u.setBankAccount(rs.getInt(5));
			}
			status = p.executeUpdate();
			connection.close();
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		return u;
		
	}
	
	 public static int update(User u) {
		 int status = 0;
		 try {
			 Connection connection = UserDao.getConnection();
			 String sql = "update user set name=?,grade=?,mobile=?,address=?,bankAddress=?";
			 PreparedStatement p = connection.prepareStatement(sql);
			 p.setString(1, u.getName());
			 p.setInt(2, u.getGrade());
			 p.setString(3, u.getMobile());
			 p.setString(4, u.getAddress());
			 p.setInt(5, u.getBankAccount());
			 
			 status= p.executeUpdate();
			 connection.close();
			 
		 }catch(Exception ex) {
			 ex.printStackTrace();
		 }
		 return status;
	 }
	
	
	
	}
