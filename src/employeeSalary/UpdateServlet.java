package employeeSalary;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/UpdateServlet")
public class UpdateServlet extends HttpServlet{
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String sbankAccount = request.getParameter("bankAccount");
		int bankAccount = Integer.parseInt(sbankAccount);
		String name = request.getParameter("name");
		String sgrade = request.getParameter("grade");
		int grade = Integer.parseInt(sgrade);
		String address = request.getParameter("address");
		String mobile = request.getParameter("mobile");

		
		User u = new User();
		u.setBankAccount(bankAccount);
		u.setName(name);
		u.setGrade(grade);
		u.setMobile(mobile);
		u.setAddress(address);
		 
		int status = UserDao.update(u);
		if(status>0) {
			response.sendRedirect("ViewServlet");
		}else {
			out.println("Sorry! Unable to update record");
		}
		
	}
	
}
